// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINT_SPHERES_AGPAOA_Paint_Spheres_AgpaoaGameMode_generated_h
#error "Paint_Spheres_AgpaoaGameMode.generated.h already included, missing '#pragma once' in Paint_Spheres_AgpaoaGameMode.h"
#endif
#define PAINT_SPHERES_AGPAOA_Paint_Spheres_AgpaoaGameMode_generated_h

#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_SPARSE_DATA
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_RPC_WRAPPERS
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaint_Spheres_AgpaoaGameMode(); \
	friend struct Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics; \
public: \
	DECLARE_CLASS(APaint_Spheres_AgpaoaGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paint_Spheres_Agpaoa"), PAINT_SPHERES_AGPAOA_API) \
	DECLARE_SERIALIZER(APaint_Spheres_AgpaoaGameMode)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaint_Spheres_AgpaoaGameMode(); \
	friend struct Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics; \
public: \
	DECLARE_CLASS(APaint_Spheres_AgpaoaGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paint_Spheres_Agpaoa"), PAINT_SPHERES_AGPAOA_API) \
	DECLARE_SERIALIZER(APaint_Spheres_AgpaoaGameMode)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PAINT_SPHERES_AGPAOA_API APaint_Spheres_AgpaoaGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaint_Spheres_AgpaoaGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINT_SPHERES_AGPAOA_API, APaint_Spheres_AgpaoaGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaint_Spheres_AgpaoaGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINT_SPHERES_AGPAOA_API APaint_Spheres_AgpaoaGameMode(APaint_Spheres_AgpaoaGameMode&&); \
	PAINT_SPHERES_AGPAOA_API APaint_Spheres_AgpaoaGameMode(const APaint_Spheres_AgpaoaGameMode&); \
public:


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINT_SPHERES_AGPAOA_API APaint_Spheres_AgpaoaGameMode(APaint_Spheres_AgpaoaGameMode&&); \
	PAINT_SPHERES_AGPAOA_API APaint_Spheres_AgpaoaGameMode(const APaint_Spheres_AgpaoaGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINT_SPHERES_AGPAOA_API, APaint_Spheres_AgpaoaGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaint_Spheres_AgpaoaGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaint_Spheres_AgpaoaGameMode)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_9_PROLOG
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_SPARSE_DATA \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_RPC_WRAPPERS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_INCLASS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_SPARSE_DATA \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINT_SPHERES_AGPAOA_API UClass* StaticClass<class APaint_Spheres_AgpaoaGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
