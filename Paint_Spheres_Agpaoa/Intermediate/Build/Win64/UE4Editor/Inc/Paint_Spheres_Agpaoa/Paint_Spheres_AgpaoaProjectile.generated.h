// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef PAINT_SPHERES_AGPAOA_Paint_Spheres_AgpaoaProjectile_generated_h
#error "Paint_Spheres_AgpaoaProjectile.generated.h already included, missing '#pragma once' in Paint_Spheres_AgpaoaProjectile.h"
#endif
#define PAINT_SPHERES_AGPAOA_Paint_Spheres_AgpaoaProjectile_generated_h

#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_SPARSE_DATA
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaint_Spheres_AgpaoaProjectile(); \
	friend struct Z_Construct_UClass_APaint_Spheres_AgpaoaProjectile_Statics; \
public: \
	DECLARE_CLASS(APaint_Spheres_AgpaoaProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paint_Spheres_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(APaint_Spheres_AgpaoaProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaint_Spheres_AgpaoaProjectile(); \
	friend struct Z_Construct_UClass_APaint_Spheres_AgpaoaProjectile_Statics; \
public: \
	DECLARE_CLASS(APaint_Spheres_AgpaoaProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paint_Spheres_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(APaint_Spheres_AgpaoaProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaint_Spheres_AgpaoaProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaint_Spheres_AgpaoaProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaint_Spheres_AgpaoaProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaint_Spheres_AgpaoaProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaint_Spheres_AgpaoaProjectile(APaint_Spheres_AgpaoaProjectile&&); \
	NO_API APaint_Spheres_AgpaoaProjectile(const APaint_Spheres_AgpaoaProjectile&); \
public:


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaint_Spheres_AgpaoaProjectile(APaint_Spheres_AgpaoaProjectile&&); \
	NO_API APaint_Spheres_AgpaoaProjectile(const APaint_Spheres_AgpaoaProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaint_Spheres_AgpaoaProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaint_Spheres_AgpaoaProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaint_Spheres_AgpaoaProjectile)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaProjectile, ProjectileMovement); }


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_9_PROLOG
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_SPARSE_DATA \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_RPC_WRAPPERS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_INCLASS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_SPARSE_DATA \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_INCLASS_NO_PURE_DECLS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINT_SPHERES_AGPAOA_API UClass* StaticClass<class APaint_Spheres_AgpaoaProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
