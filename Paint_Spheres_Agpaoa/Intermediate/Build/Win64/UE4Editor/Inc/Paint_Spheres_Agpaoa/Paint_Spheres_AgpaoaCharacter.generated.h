// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINT_SPHERES_AGPAOA_Paint_Spheres_AgpaoaCharacter_generated_h
#error "Paint_Spheres_AgpaoaCharacter.generated.h already included, missing '#pragma once' in Paint_Spheres_AgpaoaCharacter.h"
#endif
#define PAINT_SPHERES_AGPAOA_Paint_Spheres_AgpaoaCharacter_generated_h

#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_SPARSE_DATA
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_RPC_WRAPPERS
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaint_Spheres_AgpaoaCharacter(); \
	friend struct Z_Construct_UClass_APaint_Spheres_AgpaoaCharacter_Statics; \
public: \
	DECLARE_CLASS(APaint_Spheres_AgpaoaCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paint_Spheres_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(APaint_Spheres_AgpaoaCharacter)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPaint_Spheres_AgpaoaCharacter(); \
	friend struct Z_Construct_UClass_APaint_Spheres_AgpaoaCharacter_Statics; \
public: \
	DECLARE_CLASS(APaint_Spheres_AgpaoaCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paint_Spheres_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(APaint_Spheres_AgpaoaCharacter)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaint_Spheres_AgpaoaCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaint_Spheres_AgpaoaCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaint_Spheres_AgpaoaCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaint_Spheres_AgpaoaCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaint_Spheres_AgpaoaCharacter(APaint_Spheres_AgpaoaCharacter&&); \
	NO_API APaint_Spheres_AgpaoaCharacter(const APaint_Spheres_AgpaoaCharacter&); \
public:


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaint_Spheres_AgpaoaCharacter(APaint_Spheres_AgpaoaCharacter&&); \
	NO_API APaint_Spheres_AgpaoaCharacter(const APaint_Spheres_AgpaoaCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaint_Spheres_AgpaoaCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaint_Spheres_AgpaoaCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaint_Spheres_AgpaoaCharacter)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(APaint_Spheres_AgpaoaCharacter, L_MotionController); }


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_11_PROLOG
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_SPARSE_DATA \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_RPC_WRAPPERS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_INCLASS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_SPARSE_DATA \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINT_SPHERES_AGPAOA_API UClass* StaticClass<class APaint_Spheres_AgpaoaCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Paint_Spheres_AgpaoaCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
