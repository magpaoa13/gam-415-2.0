// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINT_SPHERES_AGPAOA_Fog_generated_h
#error "Fog.generated.h already included, missing '#pragma once' in Fog.h"
#endif
#define PAINT_SPHERES_AGPAOA_Fog_generated_h

#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_SPARSE_DATA
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_RPC_WRAPPERS
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFog(); \
	friend struct Z_Construct_UClass_AFog_Statics; \
public: \
	DECLARE_CLASS(AFog, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paint_Spheres_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(AFog)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAFog(); \
	friend struct Z_Construct_UClass_AFog_Statics; \
public: \
	DECLARE_CLASS(AFog, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paint_Spheres_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(AFog)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFog(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFog) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFog); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFog); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFog(AFog&&); \
	NO_API AFog(const AFog&); \
public:


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFog(AFog&&); \
	NO_API AFog(const AFog&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFog); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFog); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFog)


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_squarePlane() { return STRUCT_OFFSET(AFog, m_squarePlane); } \
	FORCEINLINE static uint32 __PPO__m_dynamicTexture() { return STRUCT_OFFSET(AFog, m_dynamicTexture); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterial() { return STRUCT_OFFSET(AFog, m_dynamicMaterial); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterialInstance() { return STRUCT_OFFSET(AFog, m_dynamicMaterialInstance); }


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_10_PROLOG
#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_PRIVATE_PROPERTY_OFFSET \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_SPARSE_DATA \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_RPC_WRAPPERS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_INCLASS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_PRIVATE_PROPERTY_OFFSET \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_SPARSE_DATA \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_INCLASS_NO_PURE_DECLS \
	Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINT_SPHERES_AGPAOA_API UClass* StaticClass<class AFog>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paint_Spheres_Agpaoa_Source_Paint_Spheres_Agpaoa_Fog_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
