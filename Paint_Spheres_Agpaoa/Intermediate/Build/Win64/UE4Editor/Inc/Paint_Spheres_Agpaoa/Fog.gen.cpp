// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paint_Spheres_Agpaoa/Fog.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFog() {}
// Cross Module References
	PAINT_SPHERES_AGPAOA_API UClass* Z_Construct_UClass_AFog_NoRegister();
	PAINT_SPHERES_AGPAOA_API UClass* Z_Construct_UClass_AFog();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paint_Spheres_Agpaoa();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AFog::StaticRegisterNativesAFog()
	{
	}
	UClass* Z_Construct_UClass_AFog_NoRegister()
	{
		return AFog::StaticClass();
	}
	struct Z_Construct_UClass_AFog_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_squarePlane_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_squarePlane;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFog_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Paint_Spheres_Agpaoa,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Fog.h" },
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance = { "m_dynamicMaterialInstance", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFog, m_dynamicMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial = { "m_dynamicMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFog, m_dynamicMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture_MetaData[] = {
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture = { "m_dynamicTexture", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFog, m_dynamicTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane = { "m_squarePlane", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFog, m_squarePlane), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFog_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFog_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFog>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFog_Statics::ClassParams = {
		&AFog::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AFog_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFog()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFog_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFog, 3660933723);
	template<> PAINT_SPHERES_AGPAOA_API UClass* StaticClass<AFog>()
	{
		return AFog::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFog(Z_Construct_UClass_AFog, &AFog::StaticClass, TEXT("/Script/Paint_Spheres_Agpaoa"), TEXT("AFog"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFog);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
