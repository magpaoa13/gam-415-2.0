// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paint_Spheres_Agpaoa/Paint_Spheres_AgpaoaGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaint_Spheres_AgpaoaGameMode() {}
// Cross Module References
	PAINT_SPHERES_AGPAOA_API UClass* Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_NoRegister();
	PAINT_SPHERES_AGPAOA_API UClass* Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Paint_Spheres_Agpaoa();
// End Cross Module References
	void APaint_Spheres_AgpaoaGameMode::StaticRegisterNativesAPaint_Spheres_AgpaoaGameMode()
	{
	}
	UClass* Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_NoRegister()
	{
		return APaint_Spheres_AgpaoaGameMode::StaticClass();
	}
	struct Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Paint_Spheres_Agpaoa,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Paint_Spheres_AgpaoaGameMode.h" },
		{ "ModuleRelativePath", "Paint_Spheres_AgpaoaGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APaint_Spheres_AgpaoaGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics::ClassParams = {
		&APaint_Spheres_AgpaoaGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaint_Spheres_AgpaoaGameMode, 997656575);
	template<> PAINT_SPHERES_AGPAOA_API UClass* StaticClass<APaint_Spheres_AgpaoaGameMode>()
	{
		return APaint_Spheres_AgpaoaGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaint_Spheres_AgpaoaGameMode(Z_Construct_UClass_APaint_Spheres_AgpaoaGameMode, &APaint_Spheres_AgpaoaGameMode::StaticClass, TEXT("/Script/Paint_Spheres_Agpaoa"), TEXT("APaint_Spheres_AgpaoaGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaint_Spheres_AgpaoaGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
